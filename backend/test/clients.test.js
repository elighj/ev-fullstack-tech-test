const request = require('supertest');
const {app} = require("../src");

const testClient = {
  name: 'Test Client 1',
  email: 'test1@test.com',
  company: 'Test Company'
}

let testId;

describe('Clients API Unit tests', () => {
  it ('create new client', async () => {
    const res = await request(app).post('/clients', testClient);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('id');
    expect(res.body).toHaveProperty('createdDate');

    testId = res.body.id;
  });

  it('get specific client', async () => {
    const res = await request(app).get(`/clients/${testId}`);
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('name');
    expect(res.body.name).toEqual(testClient.name);
    expect(res.body).toHaveProperty('email');
    expect(res.body.email).toEqual(testClient.email);
    expect(res.body).toHaveProperty('company');
    expect(res.body.company).toEqual(testClient.company);
  });

  it('get all clients', async () => {
    const res = await request(app).get('/clients');
    expect(res.statusCode).toEqual(200);
    expect(res.body.length).toBeGreaterThan(0);
  });
});
