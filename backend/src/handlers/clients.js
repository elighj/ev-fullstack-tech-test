const get_all_clients = `
  SELECT *
  FROM clients
  ;
`

const get_client = `
  SELECT *
  FROM clients
  WHERE id = ?
  ;
`

const create_client = `
  INSERT INTO clients (id, name, email, createdDate, company)
  VALUES (?, ?, ?, ?, ?)
  ;
`

const getAllClients = async () => {
  // db call here
}

const getClient = async (clientID) => {
  // db call here
}

const createClient = async (newClientId, client) => {
  const createdDate = new Date().toISOString();
  // db INSERT call here
}


module.exports = { getAllClients, getClient, createClient }
