const express = require("express");
const uuid = require('uuid').v4

const app = express();

app.listen(3001, () => {
  console.log("Server started on port 3001");
});

app.get('/clients', async (req, res) => {
  // Gets all clients
  res.json({
    status: 'OK',
  })
});

app.get('/clients/:id', async (req, res) => {
  const clientId = req.params['id'];
  // Gets specific client
  res.json({
    status: 'OK',
    body: clientId
  })
});

app.post('/clients', async (req, res) => {
  const newClientId = uuid();

  // Creates new client
  res.json({
    status: 'OK',
    body: newClientId
  })
})

module.exports = { app }
